# Traffic Meister

This is the React implementation of Sytac's test assignment "Traffic Meister".

~~This project took me 1.5 hours just to demonstrate my React skills.~~

Kidding. I've spent quite a time because I was playing with unfamiliar
technologies and wanted to know how far I can go.

## Objectives and limitations

I had set the below objectives and limitations before started working on the app:

* Typesafe React + Typescript solution. Used the current project as an
opportunity to try TS with React.
* App fully restores it's state by the URL.
User can refresh the page any time and see the same result.
User can bookmark the page, open it in another browser.
* Adequate error handling. Application shows friendly error message
and offers solution (Refresh button). To ensure error handling
works nice and fine backend emits 500 error 20% of the time.
* E2E tested. Used the project as an opportunity to try Cypress.

## React + Redux + Epics + Typescript

You can find a lot of React + Redux + Typescript implementations on GitHub
but most of them provide non-typesafe solutions. Which means an `any`
param somewhere in the redux implementation mitigates most of Typescript benefits.
The provided solution lacks of such a drawback.

Build based on this article
https://medium.com/@martin_hotell/improved-redux-type-safety-with-typescript-2-8-2c11a8062575

Post mortum: https://github.com/piotrwitek/typesafe-actions also looks nice and clean.

## Installation

```bash
$ yarn
```

## Running

```bash
$ yarn dev
```

This command starts both: frontend and backend.

I didn't concentrate on backend so there is no watchers and automatic reload implemented.

## Testing

**Jest**

```bash
$ yarn test
```

**Cypress**

```bash
$ yarn cypress:run
```

## Redux is not needed here

The first version of the project has been implemented without Redux
because the project is too simple to bring Redux
in. But I wanted to know more about typesafe + asynchronous  Redux
implementation with Typescript so here it is. You can find the first
version in `no-redux` branch.

## Code imperfections

**Absence of environment-dependent configuration**

Look at `ToDo` in [src/utils/api.tsx](src/utils/api.tsx) for better understanding.

**The selected Redux FSA library doesn't support Saga and Thunk**

I didn't find any decent Thunk implementation within Redux + TS ecosystem.

And here is the Saga issue I created for `rex-tils` library https://github.com/Hotell/rex-tils/issues/21

`typesafe-actions` project doesn't have problems with Saga.
