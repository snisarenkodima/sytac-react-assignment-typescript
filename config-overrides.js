module.exports = {
  jest: config => {
    return {
      ...config,
      transformIgnorePatterns: ["/!node_modules\\/lodash-es/"]
    };
  }
};
