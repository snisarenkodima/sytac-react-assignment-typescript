/// <reference types="Cypress" />

context("Home Page", () => {
  beforeEach(() => {
    cy.server({
      method: "GET",
      delay: 0,
      status: 200,
      response: "fixture:vehicles/full.json"
    });

    cy.route({
      url: "/vehicles**"
    });
  });

  describe("Loading indicator", () => {
    it("should display loading indicator", () => {
      cy.route({
        url: "/vehicles**",
        delay: 5000
      });
      cy.visit("/");
      cy.get('[data-test="loading-indicator"]').should("exist");
    });
  });

  describe("Error message", () => {
    it("should display error message and no content on first loading error", () => {
      cy.route({
        url: "/vehicles**",
        status: 502
      });

      cy.visit("/");
      cy.get('[data-test="loading-error"]').should("exist");
    });

    it("should display error message, disabled filter and previous content on further loading error and reload data on reload button click", () => {
      cy.visit("/");

      cy.get('[data-test="filter"]').should("exist");
      const previousVehicles = cy.get('[data-test="vehicles"]');
      previousVehicles.should("exist");
      cy.get('[data-test="loading-indicator"]').should("not.exist");
      cy.get('[data-test="loading-error"]').should("not.exist");

      cy.route({
        url: "/vehicles**",
        status: 502
      });

      cy.get('[name="type"]').select("airplane");

      cy.get('[data-test="loading-error"]').should("exist");
      cy.get('[data-test="vehicles"]').should("exist");
      cy.get('[data-test="filter"]').should("exist");
      cy.get('[name="type"]').should("be.disabled");
      cy.get('[name="brand"]').should("be.disabled");
      cy.get('[name="color"]').should("be.disabled");

      cy.route({
        url: "/vehicles**",
        status: 200,
        response: "fixture:vehicles/airplane.json"
      });

      cy.get('[data-test="reload-button"]').click();

      cy.get('[data-test="loading-error"]').should("not.exist");
      cy.get('[data-test="vehicles"]').should("exist");
      cy.get('[data-test="filter"]').should("exist");
      cy.get('[name="type"]').should("not.be.disabled");
      cy.get('[name="brand"]').should("not.be.disabled");
      cy.get('[name="color"]').should("not.be.disabled");
    });
  });

  describe("Dynamic routing", () => {
    it("Should change page URL when type has been changed", () => {
      cy.visit("/");

      // Filter airplane
      cy.route({
        url: "/vehicles?type=airplane",
        status: 200,
        response: "fixture:vehicles/airplane.json"
      }).as("airplane");
      cy.get('[name="type"]').select("airplane");
      cy.location("search").should("eq", "?type=airplane");
      cy.wait("@airplane")
        .its("url")
        .location("search")
        .should("eq", "?type=airplane");

      // Filter Boeing
      cy.route({
        url: "/vehicles?brand=Boeing 787 Dreamliner&type=airplane",
        status: 200,
        response: "fixture:vehicles/boeing.json"
      }).as("boeing");
      cy.get('[name="brand"]').select("Boeing 787 Dreamliner");
      cy.location("search").should(
        "eq",
        "?brand=Boeing%20787%20Dreamliner&type=airplane"
      );
      cy.wait("@boeing")
        .its("url")
        .location("search")
        .should("eq", "?brand=Boeing%20787%20Dreamliner&type=airplane");

      // Filter red
      cy.route({
        url: "/vehicles?brand=Boeing 787 Dreamliner&colors[]=red&type=airplane",
        status: 200,
        response: "fixture:vehicles/boeing.json"
      }).as("boeingRed");
      cy.get('[name="color"][value="red"]').check();
      cy.location("search").should(
        "eq",
        "?brand=Boeing%20787%20Dreamliner&colors[]=red&type=airplane"
      );
      cy.wait("@boeingRed")
        .its("url")
        .location("search")
        .should(
          "eq",
          "?brand=Boeing%20787%20Dreamliner&colors[]=red&type=airplane"
        );

      // Filter green
      cy.route({
        url:
          "/vehicles?brand=Boeing 787 Dreamliner&colors[]=red&colors[]=green&type=airplane",
        status: 200,
        response: "fixture:vehicles/boeing.json"
      }).as("boeingRedGreen");
      cy.get('[name="color"][value="green"]').check();
      cy.location("search").should(
        "eq",
        "?brand=Boeing%20787%20Dreamliner&colors[]=red&colors[]=green&type=airplane"
      );
      cy.wait("@boeingRedGreen")
        .its("url")
        .location("search")
        .should(
          "eq",
          "?brand=Boeing%20787%20Dreamliner&colors[]=red&colors[]=green&type=airplane"
        );

      //Exclude red
      cy.route({
        url:
          "/vehicles?brand=Boeing 787 Dreamliner&colors[]=green&type=airplane",
        status: 200,
        response: "fixture:vehicles/boeing.json"
      }).as("boeingGreen");
      cy.get('[name="color"][value="red"]').uncheck();
      cy.location("search").should(
        "eq",
        "?brand=Boeing%20787%20Dreamliner&colors[]=green&type=airplane"
      );
      cy.wait("@boeingGreen")
        .its("url")
        .location("search")
        .should(
          "eq",
          "?brand=Boeing%20787%20Dreamliner&colors[]=green&type=airplane"
        );
    });

    it("should read filter state from URL on page load", () => {
      cy.route({
        url:
          "/vehicles?brand=Boeing 787 Dreamliner&colors[]=red&colors[]=green&type=airplane",
        status: 200,
        response: "fixture:vehicles/boeing.json"
      }).as("boeingRedGreen");

      cy.visit(
        "/?brand=Boeing%20787%20Dreamliner&colors[]=red&colors[]=green&type=airplane"
      );

      cy.wait("@boeingRedGreen")
        .its("url")
        .location("search")
        .should(
          "eq",
          "?brand=Boeing%20787%20Dreamliner&colors[]=red&colors[]=green&type=airplane"
        );
    });

    it("should display empty message on ridiculous search criteria", () => {
      cy.route({
        url: "/vehicles?brand=Amer 4-4-0&colors[]=white",
        status: 200,
        response: "fixture:vehicles/empty.json"
      }).as("empty");

      cy.visit("/?brand=Amer%204-4-0&colors[]=white");

      cy.wait("@empty");

      cy.get('[data-test="empty-message"]').should("exist");
      cy.get('[name="color"][value="white"]').should("exist");

      cy.get('[name="type"]')
        .find("option:first")
        .should("be.selected")
        .and("have.value", "");

      cy.get('[name="brand"]')
        .find("option:first")
        .should("not.be.selected")
        .and("have.value", "");

      cy.get('[name="brand"]')
        .find("option:last")
        .should("be.selected")
        .and("have.value", "Amer 4-4-0");
    });
  });
});
