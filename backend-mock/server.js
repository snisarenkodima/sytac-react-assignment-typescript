import express from "express";
import { getVehicles } from "./vehicles";

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/vehicles", (req, res, next) => {
  getVehicles(req.query)
    .then(data => {
      res.json(data);
    })
    .catch(() => {
      next();
    });
});

const port = 3001;

app.listen(port, () =>
  console.log(`Mock server is running at http://localhost:${port}`)
);
