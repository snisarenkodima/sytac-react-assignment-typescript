import { RequestFilter, VehiclesData } from "../utils/api";
import { ActionsUnion, createAction } from "@martin_hotell/rex-tils";

export const VEHICLES_FETCH = "[Vehicles] fetch";
export const VEHICLES_FETCH_SUCCESS = "[Vehicles] successfully fetched";
export const VEHICLES_FETCH_ERROR = "[Vehicles] error while fetching";

export const VehicleActions = {
  fetch: (filter: RequestFilter) => createAction(VEHICLES_FETCH, filter),
  fetchSuccess: (data: VehiclesData) =>
    createAction(VEHICLES_FETCH_SUCCESS, data),
  fetchError: () => createAction(VEHICLES_FETCH_ERROR)
};

export type VehicleActions = ActionsUnion<typeof VehicleActions>;
