import { RequestFilter, AvailableFilters } from "./api";

export function castFilters(request: RequestFilter): AvailableFilters {
  return {
    types: request.type ? [request.type] : [],
    brands: request.brand ? [request.brand] : [],
    colors: request.colors ? request.colors : []
  };
}
