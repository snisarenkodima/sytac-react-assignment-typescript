import { castFilters } from "./cast-filters";

describe("castFilters", () => {
  it("should adequately behave on empty request", () => {
    expect(castFilters({})).toEqual({
      types: [],
      brands: [],
      colors: []
    });
  });

  it("should return valid response", () => {
    expect(
      castFilters({
        type: "type",
        brand: "brand",
        colors: ["red", "green"]
      })
    ).toEqual({
      types: ["type"],
      brands: ["brand"],
      colors: ["red", "green"]
    });
  });
});
