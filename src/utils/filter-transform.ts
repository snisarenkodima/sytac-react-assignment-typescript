import { RequestFilter } from "./api";
import { omit } from "lodash-es";

export function filterTransform(
  filter: RequestFilter,
  field: string,
  value: string,
  isSelected: boolean
): RequestFilter {
  if (field !== "color" && value === "") {
    return omit(filter, field);
  }

  if (field !== "color") {
    return {
      ...filter,
      [field]: value
    };
  }

  if (isSelected) {
    return {
      ...filter,
      colors: [...(filter.colors || []), value]
    };
  } else {
    return {
      ...filter,
      colors: (filter.colors || []).filter(color => color !== value)
    };
  }
}
