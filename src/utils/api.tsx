import * as queryString from "query-string";

export interface Vehicle {
  id: number;
  type: string;
  brand: string;
  colors: string[];
  img: string;
}

export interface RequestFilter {
  type?: string;
  brand?: string;
  colors?: string[];
}

export interface AvailableFilters {
  types: string[];
  brands: string[];
  colors: string[];
}

export interface VehiclesData {
  collection: Vehicle[];
  filters: AvailableFilters;
}

// ToDo: use environment-based config for URLs
const URL_PREFIX = "http://localhost:3001";

export function fetchData(filter: RequestFilter = {}): Promise<VehiclesData> {
  const body = queryString.stringify(filter, {
    arrayFormat: "bracket"
  });

  return fetch(`${URL_PREFIX}/vehicles?${body}`).then(response => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Can't get collection collection");
    }
  });
}
