import { filterTransform } from "./filter-transform";

describe("filterTransform", () => {
  it("should set types and brands", () => {
    expect(filterTransform({}, "type", "car", true)).toEqual({ type: "car" });
    expect(filterTransform({}, "brand", "Ford Focus", true)).toEqual({
      brand: "Ford Focus"
    });
  });

  it("should set colors", () => {
    expect(filterTransform({}, "color", "red", true)).toEqual({
      colors: ["red"]
    });
    expect(
      filterTransform({ colors: ["red"] }, "color", "green", true)
    ).toEqual({ colors: ["red", "green"] });
  });

  it("should unset colors", () => {
    expect(filterTransform({ colors: ["red"] }, "color", "red", false)).toEqual(
      { colors: [] }
    );
    expect(
      filterTransform({ colors: ["red", "green"] }, "color", "green", false)
    ).toEqual({ colors: ["red"] });
    expect(
      filterTransform({ colors: ["red", "green"] }, "color", "blue", false)
    ).toEqual({ colors: ["red", "green"] });
  });
});
