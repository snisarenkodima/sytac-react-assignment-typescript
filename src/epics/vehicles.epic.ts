import { ActionsObservable } from "redux-observable";
import { VehicleActions, VEHICLES_FETCH } from "../actions/vehicles";
import { ofType } from "@martin_hotell/rex-tils";
import { from, of } from "rxjs";
import { mergeMap, map, catchError } from "rxjs/operators";
import { fetchData } from "../utils/api";

export const fetchVehiclesEpic = (action$: ActionsObservable<VehicleActions>) =>
  action$.pipe(
    ofType(VEHICLES_FETCH),
    mergeMap(action =>
      from(fetchData(action.payload)).pipe(
        map(vehiclesData => VehicleActions.fetchSuccess(vehiclesData)),
        catchError(() => of(VehicleActions.fetchError()))
      )
    )
  );
