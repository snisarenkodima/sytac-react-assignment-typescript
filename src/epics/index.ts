import { combineEpics } from "redux-observable";
import { fetchVehiclesEpic } from "./vehicles.epic";

export const rootEpic = combineEpics(fetchVehiclesEpic);
