import "bootswatch/dist/minty/bootstrap.css";
import * as React from "react";
import * as ReactDOM from "react-dom";
import HomePage from "./components/HomePage";
import registerServiceWorker from "./registerServiceWorker";
import { createBrowserHistory } from "history";
import { applyMiddleware, compose, createStore } from "redux";
import {
  routerMiddleware,
  connectRouter,
  ConnectedRouter
} from "connected-react-router";
import rootReducer from "./reducers";
import { Provider } from "react-redux";
import { Route } from "react-router";
import { createEpicMiddleware } from "redux-observable";
import { rootEpic } from "./epics";

const history = createBrowserHistory();

const epicMiddleware = createEpicMiddleware();

const composeEnhancer: typeof compose =
  // tslint:disable-next-line:no-any
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  connectRouter(history)(rootReducer),
  composeEnhancer(applyMiddleware(routerMiddleware(history), epicMiddleware))
);

epicMiddleware.run(rootEpic);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route component={HomePage} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
