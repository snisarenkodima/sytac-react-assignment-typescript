import * as React from "react";
import { Alert, Button } from "reactstrap";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface ComponentProps {
  className?: string;
  loading: boolean;
  onReload: () => void;
}

class LoadingError extends React.Component<ComponentProps, {}> {
  public static defaultProps: Partial<ComponentProps> = {
    className: ""
  };

  public render() {
    const { onReload } = this.props;
    return (
      <Alert
        className={this.props.className}
        color="warning"
        data-test="loading-error"
      >
        <p>Error while data loading occurred.</p>
        <Button
          color="primary"
          onClick={onReload}
          disabled={this.props.loading}
          data-test="reload-button"
        >
          Reload
        </Button>
        {this.props.loading && (
          <FontAwesomeIcon className="ml-1" icon={faSpinner} spin={true} />
        )}
      </Alert>
    );
  }
}

export default LoadingError;
