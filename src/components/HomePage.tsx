import * as React from "react";
import { RequestFilter } from "../utils/api";
import { LoadingIndicator } from "./LoadingIndicator";
import LoadingError from "./LoadingError";
import { Col, Container, Navbar, NavbarBrand, Row } from "reactstrap";
import Filter from "./Filter";
import * as queryString from "query-string";
import Empty from "./Empty";
import { castFilters } from "../utils/cast-filters";
import { filterTransform } from "../utils/filter-transform";
import { RouteComponentProps } from "react-router";
import { connect, DispatchProp } from "react-redux";
import { State } from "../reducers";
import { isEqual } from "lodash-es";
import { VehicleActions } from "../actions/vehicles";
import { VehiclesState } from "../reducers/vehicles";

class HomePage extends React.Component<
  // tslint:disable-next-line:no-any
  RouteComponentProps<any> & StateProps & DispatchProp
> {
  public componentDidMount() {
    this.loadData();
  }

  public componentDidUpdate(prevProps: StateProps) {
    if (!isEqual(prevProps.filter, this.props.filter)) {
      this.loadData();
    }
  }

  public loadData = () => {
    this.props.dispatch(VehicleActions.fetch(this.props.filter));
  };

  public onFilterSelect = (
    field: string,
    value: string,
    isSelected: boolean
  ) => {
    this.onFilterChange(
      filterTransform(this.props.filter, field, value, isSelected)
    );
  };

  public onFilterChange(filter: RequestFilter) {
    this.props.history.push({
      search: queryString.stringify(filter, {
        arrayFormat: "bracket"
      })
    });
  }

  public render() {
    return (
      <div>
        <Navbar dark={true} expand="md" color="dark">
          <NavbarBrand href="/">The Traffic Meister</NavbarBrand>
        </Navbar>
        <Container>
          {!this.props.vehicles.loaded &&
            !this.props.vehicles.error && (
              <LoadingIndicator className="mb-5 mt-5" />
            )}
          {this.props.vehicles.error && (
            <LoadingError
              className="mb-5 mt-5"
              loading={this.props.vehicles.isLoading}
              onReload={this.loadData}
            />
          )}
          {this.props.vehicles.loaded &&
            this.props.vehicles.data && (
              <Row className="mt-4">
                <Col lg="3" className="mb-4">
                  <Filter
                    disabled={
                      this.props.vehicles.isLoading || this.props.vehicles.error
                    }
                    availableFilters={
                      this.props.vehicles.data.collection.length > 0
                        ? this.props.vehicles.data.filters
                        : castFilters(this.props.filter)
                    }
                    onSelect={this.onFilterSelect}
                    selected={this.props.filter}
                  />
                </Col>
                <Col lg="9">
                  {this.props.vehicles.data.collection.length > 0 && (
                    <Row
                      className="text-center text-lg-left"
                      data-test="vehicles"
                    >
                      {this.props.vehicles.data.collection.map(vehicle => (
                        <Col
                          key={vehicle.id}
                          lg="3"
                          md="4"
                          xs="6"
                          className="mb-4"
                        >
                          <img
                            className="img-fluid img-thumbnail"
                            src={vehicle.img}
                            alt={vehicle.brand}
                          />
                        </Col>
                      ))}
                    </Row>
                  )}
                  {this.props.vehicles.data.collection.length === 0 && (
                    <Empty />
                  )}
                </Col>
              </Row>
            )}
        </Container>
      </div>
    );
  }
}

interface StateProps {
  filter: RequestFilter;
  vehicles: VehiclesState;
}

const mapSateToProps = (state: State) => ({
  filter: queryString.parse(state.router.location.search, {
    arrayFormat: "bracket"
  }),
  vehicles: state.vehicles
});

export default connect<StateProps>(mapSateToProps)(HomePage);
