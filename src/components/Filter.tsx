import * as React from "react";
import { RequestFilter, AvailableFilters } from "../utils/api";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import { ChangeEvent } from "react";

interface ComponentProps {
  disabled: boolean;
  availableFilters: AvailableFilters;
  selected: RequestFilter;
  onSelect: (filterType: string, value: string, selected: boolean) => void;
}

class Filter extends React.Component<ComponentProps, {}> {
  public onSelect = (e: ChangeEvent<HTMLInputElement>) => {
    this.props.onSelect(e.target.name, e.target.value, e.target.checked);
  };

  public render() {
    return (
      <div data-test="filter">
        <Row>
          <Col lg={12} xs={6}>
            <FormGroup>
              <Label for="type">Type</Label>
              <Input
                type="select"
                name="type"
                disabled={this.props.disabled}
                value={this.props.selected.type}
                onChange={this.onSelect}
              >
                <option value="">All</option>
                {this.props.availableFilters.types.map(entry => (
                  <option key={entry} value={entry}>
                    {entry}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
          <Col lg={12} xs={6}>
            <FormGroup>
              <Label for="brand">Brand</Label>
              <Input
                type="select"
                name="brand"
                disabled={this.props.disabled}
                value={this.props.selected.brand}
                onChange={this.onSelect}
              >
                <option value="">All</option>
                {this.props.availableFilters.brands.map(entry => (
                  <option key={entry} value={entry}>
                    {entry}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          {this.props.availableFilters.colors.map(color => (
            <Col key={color} xs={4} sm={3} lg={12}>
              <FormGroup check={true}>
                <Label check={true}>
                  <Input
                    type="checkbox"
                    disabled={this.props.disabled}
                    name="color"
                    value={color}
                    checked={(this.props.selected.colors || []).includes(color)}
                    onChange={this.onSelect}
                  />{" "}
                  <span
                    style={{
                      backgroundColor: color,
                      width: "1em",
                      height: "1em"
                    }}
                    className="align-text-bottom border d-inline-block"
                  />{" "}
                  {color}
                </Label>
              </FormGroup>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}

export default Filter;
