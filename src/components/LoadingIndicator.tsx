import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

interface ComponentProps {
  className?: string;
}

export function LoadingIndicator(props: ComponentProps = { className: "" }) {
  return (
    <div
      className={props.className + " text-center text-muted"}
      data-test="loading-indicator"
    >
      <FontAwesomeIcon icon={faSpinner} spin={true} />
      <br />
      Loading
    </div>
  );
}
