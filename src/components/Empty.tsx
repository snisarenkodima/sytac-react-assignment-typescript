import * as React from "react";
import { Alert } from "reactstrap";

interface ComponentProps {
  className?: string;
}

class Empty extends React.Component<ComponentProps, {}> {
  public static defaultProps: Partial<ComponentProps> = {
    className: ""
  };

  public render() {
    return (
      <Alert
        className={this.props.className}
        color="info"
        data-test="empty-message"
      >
        We can not find anything corresponding to the given criteria.
      </Alert>
    );
  }
}

export default Empty;
