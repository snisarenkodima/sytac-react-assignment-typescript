import { combineReducers } from "redux";
import { RouterState } from "connected-react-router";
import { vehiclesReducer, VehiclesState } from "./vehicles";

const rootReducer = combineReducers({
  vehicles: vehiclesReducer
});

export interface State {
  router: RouterState;
  vehicles: VehiclesState;
}

export default rootReducer;
