import { VehiclesData } from "../utils/api";
import {
  VehicleActions,
  VEHICLES_FETCH,
  VEHICLES_FETCH_ERROR,
  VEHICLES_FETCH_SUCCESS
} from "../actions/vehicles";

export interface VehiclesState {
  data: VehiclesData | null;
  isLoading: boolean;
  loaded: boolean;
  error: boolean;
}

export const vehiclesReducer = (
  state: VehiclesState = {
    data: null,
    isLoading: false,
    loaded: false,
    error: false
  },
  action: VehicleActions
): VehiclesState => {
  switch (action.type) {
    case VEHICLES_FETCH:
      return {
        ...state,
        isLoading: true
      };
    case VEHICLES_FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false,
        loaded: true,
        error: false
      };
    case VEHICLES_FETCH_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    default:
      return state;
  }
};
